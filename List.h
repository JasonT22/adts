#include <iostream>
#include <stdexcept>//used to be able to "throw" exceptions
using namespace std;

#ifndef LIST_H
#define LIST_H

class List //begin List definition 
{
  private:
    class Node; //forward declaration (defined in the implementation file)
    
    Node* frontPtr = nullptr;
    int num_elements = 0;
    
  public:
     ~List();//destructor
     void insert(int element, int k);//insert element at location k
     void remove(int k);//remove element at location k
     int size();//return the number of elements in the List
     
     
     /** MISSING OPERATIONS */
     //... fill in ....
    void clear(); //added  for virtual demo purposes
};//end List definition
 
 class Node
 {
	 public:
	 int data  = 0;
	 Node* link = nullptr;
	 };
	 
  void insert(int element, int k)
 {
	 int val = element;
	 int n = k;
	 Node* newptr = new Node{val};
	 Node* frontPtr = new Node{};
	 auto iptr = frontPtr;
	 
	 for(int i = 0; i < n-1; i++)
	 {
		 iptr = iptr -> link;
	 }
	 newptr -> link = iptr-> link;
	 iptr -> link = newptr;   
 }
 void remove(int k)
 {
	int n = k;
	Node* delptr = new Node{};
	Node* frontPtr = new Node{};
	
	auto iptr = frontPtr;
	
	
	for(int i = 0; i < n-1; i++)
	{
	
	iptr = iptr -> link;
	
	}
	 delptr = iptr-> link;
	 iptr = delptr -> link;
	 int val = delptr -> data;
	 delete delptr;
	 cout <<" That element had the value was:", val <<endl;
 }

#endif


