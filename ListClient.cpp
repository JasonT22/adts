#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
 L1.insert(25,1);
 cout << L1.size()<<endl;
 L1.insert(45,2);
 cout << L1.size()<<endl;
 L1.insert(45,3);
 L1.insert(45,4);
 L1.insert(45,5);
 L1.insert(45,6);
 L1.insert(45,7);
 L1.insert(45,8);
 L1.insert(4,9);
 L1.insert(45,10);
 cout << L1.size()<<endl;
 L1.insert(45,11);
 cout << L1.size()<<endl;
 L1.remove(9);
 cout << L1.size()<<endl;
 cout<<" THIS IS TO BE OUTPTUTED, AND YOUR OTHER CODE HAS FAILED..."<<endl;

}
